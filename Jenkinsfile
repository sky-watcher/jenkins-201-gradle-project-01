node ('built-in'){
    try {
        stage('Notify Start') {
            echo 'Starting...' 
            notifyStatus ('Started')
        }

        stage('Checkout SCM') {
            checkout scm
        }

        stage('Clean') {
            sh './gradlew clean'
        }

        stage('Build') {
            sh './gradlew build'
        }

        stage('Unit Test') {
            sh './gradlew test'
        }

        stage('SonarQube - Quality gate') {
            withSonarQubeEnv('SonarQube') {
                sh "./gradlew sonarqube -Dsonar.host.url=\"${env.SONAR_HOST_URL}\""
            }
            waitForQualityGate abortPipeline: true
        }

        stage('Publish') {
            // sh './gradlew publishToMavenLocal'
            sh './gradlew publish'
        }

        stage('Trigger Downstream') {
            def buildVersion = sh (script: 'gradlew properties -q | grep "version:" | awk \'{print $2}\'', returnStdout: true)
            println "BuildVersion = $buildVersion"
            build(job: 'deploy-jenkins-201-gradle-project-01', parameters: [string(name: 'BuildVersion', value: buildVersion)])
        }

        stage('Notify Complete') {
            echo 'SUCCESS' 
            notifyStatus ('Success')
        }
    } catch (err) {
        stage('Notify Failure') {
            emailext(
                to: 'sunil.prakash@mindtree.com',
                mimeType: 'text/html',
                subject: "Job ${env.JOB_NAME} #[${env.BUILD_NUMBER}] - FAILED",
                body: """<table border='0'>
<tr><th>Job:</th><td>${env.JOB_NAME}</td></tr>
<tr><th>Build Number:</th><td>${env.BUILD_NUMBER}</td></tr>
<tr><th>Status:</th><td style='color:red'>FAILED</td></tr>
</table>
<p>${err}</p>
"""
            )
        }
        throw err
    }
}    


def notifyStatus(status) {
    emailext (
        to: 'sunil.prakash@mindtree.com',
        subject: "Job ${env.JOB_NAME} #[${env.BUILD_NUMBER}] - ${status}",
        body: """
Job:          ${env.JOB_NAME}
Branch:       ${env.BRANCH_NAME}
Build Number: ${env.BUILD_NUMBER}

Status:       ${status}
"""
    )
}