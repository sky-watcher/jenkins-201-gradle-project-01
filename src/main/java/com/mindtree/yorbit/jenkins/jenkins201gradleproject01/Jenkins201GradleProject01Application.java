package com.mindtree.yorbit.jenkins.jenkins201gradleproject01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jenkins201GradleProject01Application {

	public static void main(String[] args) {
		SpringApplication.run(Jenkins201GradleProject01Application.class, args);
	}

}
